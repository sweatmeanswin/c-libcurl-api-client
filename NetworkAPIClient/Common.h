#pragma once

#include <map>
#include <string>

using HttpHeaders = std::map<std::string, std::string>;
using HttpParams = std::map<std::string, std::string>;
