#include "APIClient.h"
#include "Common.h"

const char * COOKIE_KEY = "Cookie";

void checkCURLcode(CURLcode resCode)
{
	if (resCode == CURLE_OK) {
		return;
	}
	const char* szErrorMsg = curl_easy_strerror(resCode);
	switch (resCode)
	{
	case CURLE_FAILED_INIT:
		throw APIClientError(szErrorMsg);
	default:
		throw HttpConnectionError(szErrorMsg);
	}
}

APIClient::APIClient(const std::string & sUrl) : m_curlInstance(nullptr), m_sApiHost(sUrl) {}

APIClient::~APIClient()
{
	if (m_curlInstance != nullptr) {
		curl_easy_cleanup(m_curlInstance);
	}
}

void APIClient::Init()
{
	m_curlInstance = curl_easy_init();
	if (!m_curlInstance) {
		std::string sErrMsg;
		sErrMsg.reserve(1024);
		strerror_s(&sErrMsg[0], 1024, errno);
		throw APIClientError(sErrMsg);
	}
	curl_easy_setopt(m_curlInstance, CURLOPT_HEADER, 0L);
	curl_easy_setopt(m_curlInstance, CURLOPT_FOLLOWLOCATION, 1L);
	curl_easy_setopt(m_curlInstance, CURLOPT_HEADERFUNCTION, &APIClient::HeadersCallback);
	curl_easy_setopt(m_curlInstance, CURLOPT_READFUNCTION, &APIClient::ReadCallback);
	curl_easy_setopt(m_curlInstance, CURLOPT_WRITEFUNCTION, &APIClient::WriteCallback);
}

void APIClient::SetCookies(const std::string & sCookies)
{
	m_internalHeaders[COOKIE_KEY] = sCookies;
}

HttpResponse APIClient::Get(const std::string& sMethod, const HttpParams* Params, const HttpHeaders* Headers)
{
	curl_easy_setopt(m_curlInstance, CURLOPT_POST, 0L);
	return processRequest(sMethod, Params, Headers);
}

HttpResponse APIClient::Post(const std::string & sMethod, const HttpParams * Params, const HttpHeaders * Headers)
{
	curl_easy_setopt(m_curlInstance, CURLOPT_POST, 1L);
	return processRequest(sMethod, Params, Headers);
}

HttpResponse APIClient::Auth(
	long AuthType,
	const std::string& sUsername,
	const std::string& sPassword,
	const HttpParams* Params,
	const HttpHeaders* Headers
)
{
	curl_easy_setopt(m_curlInstance, CURLOPT_HTTPAUTH, AuthType);
	curl_easy_setopt(m_curlInstance, CURLOPT_USERNAME, sUsername.c_str());
	curl_easy_setopt(m_curlInstance, CURLOPT_PASSWORD, sPassword.c_str());
	HttpResponse response = Get("auth", Params, Headers);
	if (response.Success()) {
		auto& headers = response.GetHeaders();
		SetCookies(headers.at("Set-Cookie"));
	}
	return response;
}

void APIClient::setRequestHeaders(const HttpHeaders * pHeaders)
{
	struct curl_slist * headers = nullptr;
	if (pHeaders) {
		for (auto cit = pHeaders->cbegin(); cit != pHeaders->cend(); ++cit) {
			headers = curl_slist_append(headers, (cit->first + ": " + cit->second).c_str());
		}
	}
	// Set default headers that haven't been set
	for (auto cit = m_internalHeaders.cbegin(); cit != m_internalHeaders.cend(); ++cit) {
		if (!pHeaders || pHeaders->find(cit->first) == pHeaders->cend()) {
			headers = curl_slist_append(headers, (cit->first + ": " + cit->second).c_str());
		}
	}
	if (headers) {
		curl_easy_setopt(m_curlInstance, CURLOPT_HTTPHEADER, headers);
	}
}

HttpResponse APIClient::processRequest(const std::string & sMethod, const HttpParams * Params, const HttpHeaders * Headers)
{
	std::string responseText;
	HttpHeaders responseHeaders;

	setRequestHeaders(Headers);
	auto url = formatUrl(m_sApiHost, sMethod, Params);
	curl_easy_setopt(m_curlInstance, CURLOPT_URL, url.c_str());
	curl_easy_setopt(m_curlInstance, CURLOPT_WRITEDATA, std::ref(responseText));
	curl_easy_setopt(m_curlInstance, CURLOPT_HEADERDATA, std::ref(responseHeaders));

	CURLcode code = curl_easy_perform(m_curlInstance);
	// Check CURL internal operation result
	checkCURLcode(code);
	// Create Response object and return
	long statusCode = 0;
	curl_easy_getinfo(m_curlInstance, CURLINFO_RESPONSE_CODE, &statusCode);
	HttpResponse responseObject(statusCode, responseText, responseHeaders);
	return std::move(responseObject);
}

std::string APIClient::formatUrl(const std::string & host, const std::string & sMethod, const HttpParams * params)
{
	std::stringstream ssUrl;
	ssUrl << host;
	if (*host.rbegin() != '/')
		ssUrl << "/";
	ssUrl << sMethod << "/";

	if (params && params->size()) {
		ssUrl << "?";
		std::stringstream ssParams;
		auto cit = params->cbegin();
		ssParams << cit->first << "=" << cit->second;
		for (; ++cit != params->cend(); ) {
			ssParams << "&" << cit->first << "=" << curl_easy_escape(m_curlInstance, cit->second.c_str(), cit->second.size());
		}
		ssUrl << ssParams.str();
	}
	return ssUrl.str();
}

size_t APIClient::HeadersCallback(char * pContent, size_t size, size_t nmemb, HttpHeaders* pHeaders)
{
	if (pHeaders) {
		std::string sHeaderLine(pContent);
		size_t delimPos = sHeaderLine.find_first_of(':');
		if (delimPos != sHeaderLine.npos) {
			pHeaders->insert(std::make_pair(sHeaderLine.substr(0, delimPos), sHeaderLine.substr(delimPos + 2, sHeaderLine.size() - delimPos - 4)));
		}
	}
	return size * nmemb;
}

size_t APIClient::ReadCallback(char * pContent, size_t size, size_t nmemb, HttpParams* pParams)
{
	if (!pParams || pParams->empty()) {
		return 0;
	}
	std::stringstream ss;
	// Process HttpParams to ss buffer
	std::string sBuffer = ss.str();
	std::memcpy(pContent, sBuffer.c_str(), sBuffer.size());
	return sBuffer.size() * sizeof(char);
}

size_t APIClient::WriteCallback(char * pContent, size_t size, size_t nmemb, std::string & sGetResponse)
{
	size_t newSize = size * nmemb;
	size_t oldSize = sGetResponse.size();
	try {
		sGetResponse.reserve(oldSize + newSize);
	}
	catch (std::bad_alloc &e) {
		throw APIClient(e.what());
	}
	sGetResponse.append(pContent);
	return newSize;
}
