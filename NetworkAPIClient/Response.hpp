#pragma once

#include "Json.hpp"
#include "Errors.hpp"
#include "Common.h"

#include <string>
#include <sstream>

using namespace nlohmann;

const int HTTP_STATUS_CODE_OK = 200;

class HttpResponse {
public:
	HttpResponse(int statusCode, const std::string& text, const HttpHeaders& headers) : m_text(text), m_headers(headers), m_statusCode(statusCode)
	{
	}

	const HttpHeaders& GetHeaders() const {
		return m_headers;
	}

	const json& GetJson() {
		if (m_json.empty()) {
			try {
				m_json = json::parse(m_text);
			}
			catch (json::parse_error& parseErr) {
				throw BadJsonError(parseErr.what());
			}
			catch (BadJsonError& jsonErr) {
				throw BadJsonError(jsonErr.what());
			}
		}
		return m_json;

	}

	const std::string& GetText() const {
		return m_text;
	}

	int GetStatusCode() const {
		return m_statusCode;
	}

	void RaiseForStatus()
	{
		if (m_statusCode >= 400) {
			std::stringstream ss;
			ss << "[HTTP] Server return " << m_statusCode << " code. Data: " << m_text << std::endl;
			throw HttpConnectionError(ss.str());
		}
	}

	bool Success() const {
		return GetStatusCode() == HTTP_STATUS_CODE_OK;
	}

private:
	json m_json;
	std::string m_text;
	HttpHeaders m_headers;
	int m_statusCode;
};
