#include "APIClient.h"
#include <iostream>

int main()
{
	std::string sHost("api://server");
	APIClient client(sHost);
	auto authResponse = client.Auth(AuthenticationType::Basic, "user", "password");
	if (!authResponse.Success()) {
		std::cout << "code: " << authResponse.GetStatusCode() << ". data: " << authResponse.GetText() << std::endl;
		return -1;
	}

	auto apiJsonResponse = client.Get("method");
	try {
		apiJsonResponse.RaiseForStatus();
	}
	catch (HttpConnectionError& err) {
		std::cout << err.what() << std::endl;
		return -2;
	}
	try {
		json jsonData = apiJsonResponse.GetJson();
		std::cout << jsonData.dump(4) << std::endl;
	}
	catch (BadJsonError& err) {
		std::cout << err.what() << std::endl;
		return -3;
	}

	HttpParams params{ { "param1", "value1" } };
	HttpHeaders headers{ { "header1", "value1" } };
	auto apiPostResponse = client.Post("method", &params, &headers);
	if (!apiPostResponse.Success()) {
		std::cout << apiPostResponse.GetText() << std::endl;
		std::cout << "Headers:" << std::endl;
		auto& headers = apiPostResponse.GetHeaders();
		for (auto cit = headers.cbegin(); cit != headers.cend(); ++cit) {
			std::cout << cit->first << ": " << cit->second << std::endl;
		}
	}

	return 0;
}
