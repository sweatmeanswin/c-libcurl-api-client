#pragma once

#include "Response.hpp"

#include <curl/curl.h>

enum AuthenticationType : uint8_t {
	Basic = CURLAUTH_BASIC,
};

class APIClient
{
public:
	APIClient(const std::string& sUrl);
	~APIClient();

	void Init();
	void SetCookies(const std::string& sCookies);
	HttpResponse Get(
		const std::string& sMethod,
		const HttpParams* Params = nullptr,
		const HttpHeaders* Headers = nullptr
	);
	HttpResponse Post(
		const std::string& sMethod,
		const HttpParams* Params = nullptr,
		const HttpHeaders* Headers = nullptr
	);
	HttpResponse Auth(
		long AuthType,
		const std::string& sUsername,
		const std::string& sPassword,
		const HttpParams* Params = nullptr,
		const HttpHeaders* Headers = nullptr
	);

private:
	CURL * m_curlInstance;
	std::string m_sApiHost;
	HttpHeaders m_internalHeaders;

	void setRequestHeaders(const HttpHeaders* pHeaders);
	HttpResponse processRequest(
		const std::string& sMethod,
		const HttpParams* Params = nullptr,
		const HttpHeaders* Headers = nullptr
	);
	std::string formatUrl(const std::string & host, const std::string & sMethod, const HttpParams * params);

	static size_t HeadersCallback(char * pContent, size_t size, size_t nmemb, HttpHeaders* pHeaders);
	static size_t ReadCallback(char * pContent, size_t size, size_t nmemb, HttpParams* pParams);
	static size_t WriteCallback(char * pContent, size_t size, size_t nmemb, std::string& sGetResponse);
};

