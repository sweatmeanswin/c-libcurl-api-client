#pragma once

#include <string>
#include <exception>

class CBaseException : public std::exception {
public:
	CBaseException(const std::string& sErrorMsg) : std::exception(sErrorMsg.c_str()) {}
};

class HttpConnectionError : public CBaseException {
public:
	HttpConnectionError(const std::string& sErrorMsg) : CBaseException(sErrorMsg) {}
};
class APIClientError : public CBaseException {
public:
	APIClientError(const std::string& sErrorMsg) : CBaseException(sErrorMsg) {}
};
class BadJsonError : public CBaseException {
public:
	BadJsonError(const std::string& sErrorMsg) : CBaseException(sErrorMsg) {}
};
